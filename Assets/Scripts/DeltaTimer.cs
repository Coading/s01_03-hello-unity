﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeltaTimer : MonoBehaviour
{
    float a = 0f;
    float b = 0f;

    float CounterA ()
    {
        // 프레임당 하나씩 더하는 경우
        return a += 1.0f;
    }

    float CounterB ()
    {
        // 초당 하나씩 더하는 경우
        return b += 1.0f * Time.deltaTime;
    }

    void Update()
    {
        Debug.LogFormat("{0},{1}", (int)CounterA(), (int)CounterB());
    }
}
    