﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveTween : MonoBehaviour
{
    private Ease old_easyType;
    public Ease easyType;
    public float time = 1f;
    private Vector3 oldPosition;

    private void Awake()
    {
        oldPosition = this.transform.localPosition;
        old_easyType = Ease.Unset;
        easyType = Ease.Unset;
    }

    void Update ()
    {
        if (old_easyType != easyType)
        {
            old_easyType = easyType;
            transform.localPosition = oldPosition;

            transform.DOKill();

            if (easyType != Ease.Unset)
                transform.DOLocalMoveX(280f, time).SetLoops(-1, LoopType.Yoyo).SetEase(easyType);
        }
    }
}
